<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Http\Requests\GetAllArticlesRequest;
use App\Http\Requests\ArticleFeedRequest;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

class ArticleController extends Controller
{

    public function __construct(){
        $this->middleware('auth:api', ['except' => [
            'getSingleArticle',
            'getAllArticles',
        ]]);
    }

    public function getSingleArticle(string $slug){
        $article = Article::where('slug', $slug)->first();

        if ($article === null){
            return response()->json(['error' => 'Not found'], 404);
        }
        return new ArticleResource($article);
    }

    public function createArticle(CreateArticleRequest $request){
        $input = $request->validated();

        $input['article']['author_id'] = auth()->id();

        $article = Article::create($input['article']);

        $tags = $input['article']['tagList'];
        $attachTags = [];
        foreach ($tags as $tag){
            $tag = Tag::firstOrCreate(['tag' => $tag]);
            array_push($attachTags, $tag);
        }
        $article->tags()->saveMany($attachTags);
        return new ArticleResource($article);
    }

    public function updateArticle(UpdateArticleRequest $request, string $slug){
        $input = $request->validated();
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'Article not found'], 404);
        }
        if ($article->author_id !== auth()->id()){
            return response()->json(['error' => 'Unauthorised'], 401);
        }
        if(Arr::exists($input['article'], 'title')){
            $input['article']['slug'] = Str::slug($input['article']['title'], '-');
        }
        if (Arr::exists($input['article'], 'tagList')){
            $tags = $input['article']['tagList'];
            foreach ($tags as $tag){
                $exists = Tag::where('tag', $tag)->exists();
                if($exists){
                    $contains = Tag::where('tag', $tag)->first()->hasArticle($article->id);
                }
                if((!$exists && !isset($contains)) || (!$exists && !$contains)) {
                    Tag::create(['tag' => $tag])->articles()->attach($article->id);
                } else if(!$contains){
                    Tag::where('tag', $tag)->first()->articles()->attach($article->id);
                }
            }
        }
        $article->update($input['article']);
        $article = $article->fresh();
        return new ArticleResource($article);
    }

    public function getAllArticles(GetAllArticlesRequest $request){
        $limit = $request->query('limit', 20);
        $offset = $request->query('offset', 0);
        $articles = new Article();
        if($request->query('author')){
            $author = User::where('username', $request->query('author'))->first();
            $articles = $articles->where('author_id', $author->id);
        }
        if ($request->query('favorited')){
            $user = User::where('username', $request->query('favorited'))->first();
            $articles = $articles->whereHas('favorited', function (Builder $query) use ($user){
                $query->where('user_id', $user->id);
            });
        }
        if ($request->query('tag')){
            $tag = Tag::where('tag', $request->query('tag'))->first();
            $articles = $articles->whereHas('tags', function (Builder $query) use ($tag){
                $query->where('tag', $tag->tag);
            });
        }
        if ($articles === null){
            return response()->json(['error' => 'Articles not found'], 404);
        }
        $articles = $articles
            ->limit($limit)
            ->offset($offset)
            ->latest()
            ->get();

        return new ArticleCollection($articles);
    }

    public function articleFeed(ArticleFeedRequest $request){
        $limit = $request->query('limit', 20);
        $offset = $request->query('offset', 0);

        $follows = auth()->user()->follows()->pluck('author_id');
        if ($follows->count() === 0){
            return response()->json(['error' => 'Users not found'], 404);
        }
        $articles = Article::whereIn('author_id', $follows)
            ->limit($limit)
            ->offset($offset)
            ->latest()
            ->get();

        if ($articles === null){
            return response()->json(['error' => 'Articles not found'], 404);
        }
        return new ArticleCollection($articles);
    }

    public function deleteArticle(string $slug){
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'Article not found'], 404);
        }
            $article->delete();
        if ($article->author_id !== auth()->id()){
            return response()->json(['error' => 'Unauthorised'], 401);
        }
        return response()->json([
            'article' => 'Article removed'
        ]);
    }

    public function favoriteArticle(string $slug){
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'article not found'], 404);
        }
        $following = auth()->user()->favoriteArticles()->where('article_id', $article->id)->exists();
        if (!$following){
            $article->favorited()->save(auth()->user());
            return new ArticleResource($article);
        }
        return response()->json(['error' => 'Already favorited']);
    }

    public function unfavoriteArticle(string $slug){
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'article not found'], 404);
        }
        $following = auth()->user()->favoriteArticles()->where('article_id', $article->id)->exists();
        if ($following){
            $article->favorited()->detach(auth()->user());
            return new ArticleResource($article);
        }
        return response()->json(['error' => 'No yet favorited']);
    }
}
