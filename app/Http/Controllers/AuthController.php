<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;

class AuthController extends Controller
{
    public function register(RegisterRequest $request){

        $input = $request->validated();

        $input['user']['bio'] = '';
        $input['user']['image'] = '';

        $user = User::create($input['user']);
        $user = $user->fresh();

        $credentials = $request->all()['user'];
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user->token = $token;
        return new UserResource($user);
    }

    public function login(LoginRequest $request){
        $credentials = $request->all()['user'];

        $user = User::where('email', $credentials['email'])->first();

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Credentials incorrect'], 401);
        }

        $user->token = $token;

        return new UserResource($user);
    }
}
