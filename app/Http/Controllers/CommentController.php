<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Models\Article;
use App\Models\Comment;
use App\Models\User;

class CommentController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api', ['except' => 'getComments']);
    }

    public function createComment(CreateCommentRequest $request, string $slug){
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'Article not found'], 404);
        }
        $input = $request->validated();
        $input['comment']['author_id'] = auth()->id();
        $input['comment']['article_id'] = $article->id;
        $comment = Comment::create($input['comment']);
        $comment->author = auth()->user();
        return response()->json([
            'comment' => $comment
        ]);
    }

    public function deleteComment(string $slug, int $id){
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'Article not found'], 404);
        }
        $comment = Comment::where('id', $id)->first();
        if ($comment === null){
            return response()->json(['error' => 'Comment not found'], 404);
        }
        if ($comment->author_id !== auth()->id()){
            return response()->json(['error' => 'Unauthorised'], 401);
        }
        $comment->delete();
        return response()->json([
            'comment' => 'Comment removed'
        ]);
    }

    public function getComments(string $slug){
        $article = Article::where('slug', $slug)->first();
        if ($article === null){
            return response()->json(['error' => 'Article not found'], 404);
        }
        $comments = Comment::where('article_id', $article->id)->get();
        $commentsWithAuthor = [];
        foreach ($comments as $comment){
            $comment->author = User::where('id', $comment->author_id)->first();
            array_push($commentsWithAuthor, $comment);
        }
        return response()->json([
           'comments' => $comments
        ]);
    }
}
