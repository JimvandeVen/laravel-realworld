<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function showUser(string $username){
        $profile = User::where('username', $username)->first();

        if ($profile === null) {
            return response()->json(['error' => 'User not found'], 404);
        }
        $followingId = $profile->id;
        $following = auth()->user()->follows()->where('author_id',$followingId)->exists();
        $profile->following = $following;

        return response()->json([
            'profile'=> $profile,
        ]);
    }

    public function getCurrentUser(){
        $token = request()->bearerToken();
        $user = User::where('id', auth()->id())->first();

        $user->token = $token;
        return response()->json([
            'user'=> $user ,
        ]);
    }

    public function updateUser(UpdateUserRequest $request){

        $token = request()->bearerToken();
        $input = $request->validated();
        $user = auth()->user();
        $user->update($input['user']);
        $user = $user->fresh();
        $user->token = $token;

        return response()->json([
            'user'=> $user
        ]);
    }

    public function followUser(string $username){

        $profile = User::where('username', $username)->first();

        $followingId = $profile->id;
        $following = auth()->user()->follows()->where('author_id',$followingId)->exists();
        if(!$following){
            auth()->user()->follows()->attach($followingId);
            $following = auth()->user()->follows()->where('author_id',$followingId)->exists();
            $profile->following = $following;
            return response()->json([
                'profile'=> $profile,
            ]);
        } else {
            return response()->json(['error' => 'Already following'], 401);
        }
    }

    public function unFollowUser(string $username){

        $profile = User::where('username', $username)->first();

        $followingId = $profile->id;
        $following = auth()->user()->follows()->where('author_id',$followingId)->exists();
        if($following){
            auth()->user()->follows()->detach($followingId);
            $following = auth()->user()->follows()->where('author_id',$followingId)->exists();
            $profile->following = $following;
            return response()->json([
                'profile'=> $profile,
            ]);
        } else {
            return response()->json(['error' => 'Not yet following'], 401);
        }
    }
}


