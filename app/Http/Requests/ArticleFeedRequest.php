<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleFeedRequest extends FormRequest
{
    public function rules()
    {
        return [
            'limit' => 'integer',
            'offset' => 'integer',
            'author' => ['string', 'exists:App\Models\User,username'],
        ];
    }
}
