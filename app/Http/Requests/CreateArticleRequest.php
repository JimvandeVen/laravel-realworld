<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateArticleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'article.title' => ['max:100', 'required', 'unique:App\Models\Article,title'],
            'article.description' => ['max:20', 'required'],
            'article.body' => ['max:500', 'required'],
            'article.tagList' => ['array']
        ];
    }
}
