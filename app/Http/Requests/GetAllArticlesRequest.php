<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetAllArticlesRequest extends FormRequest
{
    public function rules()
    {
        return [
            'limit' => 'integer',
            'offset' => 'integer',
            'author' => ['string', 'exists:App\Models\User,username'],
            'favorited' => ['string', 'exists:App\Models\User,username'],
            'tag' => ['string', 'exists:App\Models\Tag,tag']
        ];
    }
}
