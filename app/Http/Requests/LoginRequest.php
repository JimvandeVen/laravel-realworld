<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user.email' => ['required', 'exists:App\Models\User,email'],
            'user.password' => 'required',
        ];
    }
}
