<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function rules()
    {
        return [

                'user.username' => ['required', 'regex:/^[A-Za-z][A-Za-z0-9]{5,31}$/', 'unique:App\Models\User,username'],
                'user.email' => ['required', 'email', 'unique:App\Models\User,email'],
                'user.password' => ['required', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/'],

        ];
    }
}
