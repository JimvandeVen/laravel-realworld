<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'article.title' => ['max:100', 'unique:App\Models\Article,title'],
            'article.description' => ['max:20'],
            'article.body' => ['max:500'],
            'article.tagList' => ['array']
        ];
    }
}
