<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{

    public function rules()
    {
        return [
            'user.username' => ['regex:/^[A-Za-z][A-Za-z0-9]{5,31}$/', 'unique:App\Models\User,username'],
            'user.email' => ['unique:App\Models\User,email'],
            'user.bio' => ['max:100'],
            'user.image' => ['active_url'],
            'user.password' => ['regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/'],
        ];
    }
}
