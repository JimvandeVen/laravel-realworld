<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ArticleCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public static $wrap = 'articles';

    public function toArray($request)
    {
        return [
            'articlesCount' => $this->count(),
            'articles' => $this->collection,
        ];
    }
}
