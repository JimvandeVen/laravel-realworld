<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'article';

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'description' => $this->description,
            'body' => $this->body,
            'tagList' => $this->tags->pluck('tag'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'favoritesCount' => $this->favorited()->count(),
            'favorited' => $this->favorited->contains(auth()->id()),
            'author' => $this->author,
        ];
    }
}
