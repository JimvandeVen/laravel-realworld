<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $with = ['author', 'favorited', 'tags', 'comments', ];

    public function author(){
        return $this->belongsTo(User::class, 'author_id');
    }

    public function favorited(){
        return $this->belongsToMany(User::class, 'favorite_articles');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class, 'tagged');
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function titleToSlug($title){
         return Str::slug($title, '-');
    }

    protected $fillable = [
        'slug',
        'title',
        'description',
        'body',
        'author_id',
    ];
}
