<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    public function articles(){
        return $this->belongsToMany(Article::class, 'tagged');
    }

    public function hasArticle($article) {
        return $this->articles->contains($article);
    }

    protected $fillable =[
        'tag'
    ];
}
