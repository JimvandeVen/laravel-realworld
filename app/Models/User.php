<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'email',
        'password',
        'username',
        'bio',
        'image',
    ];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
    ];

    public function follows(){
        return $this->belongsToMany(User::class, 'subscriptions', 'subscriber_id', 'author_id');
    }

    public function followers(){
        return $this->belongsToMany(User::class, 'subscriptions', 'author_id', 'subscriber_id');
    }

    public function writtenArticles(){
        return $this->hasMany(Article::class, 'author_id', 'id');
    }

    public function favoriteArticles(){
        return $this->belongsToMany(Article::class, 'favorite_articles');
    }

    public function comments(){
        return $this->hasMany(Comment::class, 'author_id', 'id');
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
