<?php

namespace App\Observers;

use App\Models\Article;
use App\Models\Tag;

class ArticleObserver
{
    public function saving(Article $article)
    {
        $article->slug = $article->titleToSlug($article->title);
    }

    public function deleting(Article $article)
    {
        $tags = $article->tags()->get();
        $article->favorited()->detach();
        $article->tags()->detach();
        $article->comments()->delete();
        forEach ($tags as $tag){
            if (!$tag->articles()->first()){
                $tag->delete();
            }
        }



    }

}
