<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    protected $model = Article::class;
    public function definition()
    {

        $title = $this->faker->unique()->realText(100, '5');
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'description' => $this->faker->realText(20),
            'body' => $this->faker->realText(500),
        ];
    }
}
