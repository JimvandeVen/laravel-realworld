<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ArticleSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();

        $users->each(function($user){
            $numbers = collect([1,2,3,4,5]);

            Article::factory()
                ->count(2)
                ->for($user, 'author')
                ->hasFavorited($numbers->random())
                ->hasTags($numbers->random())
                ->create();
        });
    }
}
