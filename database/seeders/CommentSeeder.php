<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    public function run()
    {
        $articles = Article::all();
        $users = User::all();

        $articles->each(function($article) use ($users){
            $user = $users->random();
            $numbers = collect([1,2,3,4,5]);
            Comment::factory()
                ->count($numbers->random())
                ->for($article, 'article')
                ->for($user, 'author')
                ->create();
        });
    }
}
