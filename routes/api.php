<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\CommentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('users/login', [AuthController::class, 'login']);
Route::post('users', [AuthController::class, 'register']);

Route::group([

    'middleware' => 'api'

], function () {
    Route::get('user', [UserController::class, 'getCurrentUser']);
    Route::put('user', [UserController::class, 'updateUser']);
    Route::get('/profiles/{username}', [UserController::class, 'showUser']);
    Route::post('/profiles/{username}/follow', [UserController::class, 'followUser']);
    Route::delete('/profiles/{username}/follow', [UserController::class, 'unFollowUser']);

    Route::get('/articles', [ArticleController::class, 'getAllArticles']);
    Route::get('/articles/feed', [ArticleController::class, 'articleFeed']);
    Route::get('/articles/{slug}', [ArticleController::class, 'getSingleArticle']);
    Route::post('/articles',[ArticleController::class, 'createArticle']);
    Route::put('/articles/{slug}', [ArticleController::class, 'updateArticle']);
    Route::delete('articles/{slug}', [ArticleController::class, 'deleteArticle']);
    Route::post('articles/{slug}/favorite', [ArticleController::class, 'favoriteArticle']);
    Route::delete('articles/{slug}/favorite', [ArticleController::class, 'unfavoriteArticle']);

    Route::get('/tags', [TagController::class, 'getTags']);

    Route::post('articles/{slug}/comments', [CommentController::class, 'createComment']);
    Route::delete('articles/{slug}/comments/{id}', [CommentController::class, 'deleteComment']);
    Route::get('articles/{slug}/comments', [CommentController::class, 'getComments']);
});
