<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/users', [UserController::class, 'users']);
// or



Route::get('/', function () {
    return view('welcome');
});
//
//Route::get('/user', [Controller::class, 'index']);
//
//Route::get('/users', function(request $request){
//   //....
//});
//
//Route::redirect('here', 'there', 301);
//
//Route::view('/welcome', 'welcome');
//
//Route::get('/user/{id}', function($id){
//    return 'User '.$id;
//});
//
//Route::get('/posts/{post}/comments/{comment}', function($postId, $commentId){
//    //.....
//});
//
//Route::get('/user/{name?}', function($name='john'){
//    return $name;
//});
//
//Route::get('/user/{name}', function($name){
//    //....
//})->where('name', '[A-Za-z]+');
//
//
//Route::get('/user/profile', function(){
//    //......
//})->name('profile');
//
//Route::get('/profile', function(){
//    return redirect()->route('profile');
//});
//
//Route::middleware(['first', 'second'])->group(function(){
//    Route::get('/', function(){
//        //....
//    });
//    Route::get('/user/profile', function(){
//        //.......
//    });
//});
//
//Route::prefix('admin')->group(function(){
//    Route::get('users', function(){
//        // URL = "/admin/users"
//    });
//});



